An alternative to file-based mentorship is commit history based
mentorship. This is a version of project-based mentorship but we do
not need to worry about the project identity/forking.

The presumtion is that someone making a commit may be aware of the
parent commits and that authors of parent commits transfer
their knowledge to authors of child commits.

The measure may need refinment as the granularity of commit history
is not as refined as history of individual source code files, but it
certainly is simpler to compute.


```
#First create a fast lookup for the content of each commit
perl Cmt2Cont.perl

#next map commits to their parents
perl Cmt2Par1.perl

#to verify lets also map commits to their children
perl Cmt2Par.perl
#export as flat file:
perl ~/bin/dumpC2C.perl /fast1/All.sha1c/commit_child.tch |gzip > c2c.gz


# and commits to to authors
perl Cmt2Auth.perl
# and authors to commits
perl Auth2Cmt.perl

#now we can get the author to author map for the immediate paerents
for i in {0.127}
do perl -I ~/lib64/perl5/ Cmt2ParN.perl $i | gzip > Cmt2ParN.$i.gz
done

#The following is creating cmt to project map (I also have a script running for commit to file)
perl -I ~/lib64/perl5 Cmt2Prj.perl

```

The output in Cmt2ParN.*.gz contains author to mentor mapping
based on an immediate parent of a commit. Tracing it further
can produce an arbitrary number of times.
Note that parent commits by the same author are ignored.

Each commit to parent sequence of commits can be counted to
detect tjhe most likely subset of mentors and, the lines with too
many authors/commits, may be downweighted as with file mentorship
measure.

```
gunzip -c Cmt2ParN.*.gz|  \
 perl -e 'while(<STDIN>){chop();@x=split(/\;/);next if $#x < 3;$m{$x[0]}{$x[1]}++};while (($k,$v)=each %m){for $mm (keys %{$v}){print "$k\;$mm\;$v->{$mm}\n";}}' | \
  gzip > mentorCmt.gz
```

Some top mentors:
```
gunzip -c mentorCmt.gz | grep -v '^;' | lsort 20G -rn -t\; -k3 | head
cmsbuild <>;William Tanenbaum <sha1-faefd172b4369b2002e8f0f7d11f304fd9896b5b@cern.ch>;43082
root <root@f38db490-d61c-443f-a65b-d21fe96a405b>;nobody <nobody@f38db490-d61c-443f-a65b-d21fe96a405b>;37250
nobody <nobody@f38db490-d61c-443f-a65b-d21fe96a405b>;root <root@f38db490-d61c-443f-a65b-d21fe96a405b>;36052
Github Test <githubtest@jetbrains.com>;Aleksey Pivovarov <Aleksey.Pivovarov@jetbrains.com>;35695
ideatest1 <githubtest1@gmail.com>;Aleksey Pivovarov <Aleksey.Pivovarov@jetbrains.com>;25566
ideatest1 <githubtest1@gmail.com>;Github Test <githubtest@jetbrains.com>;25562
Matthew Weier O'Phinney <matthew@zend.com>;Ralph Schindler <ralph.schindler@zend.com>;17129
OpenStack Proposal Bot <openstack-infra@lists.openstack.org>;Jenkins <jenkins@review.openstack.org>;15776
thekid <timm.friebe@gmail.com>;kiesel <alex@kiesel.name>;15187
kiesel <alex@kiesel.name>;thekid <timm.friebe@gmail.com>;14848
```


The content of Cmt2ParN.perl
```
use strict;
use warnings;
use TokyoCabinet;
use Compress::LZF;

sub toHex { 
        return unpack "H*", $_[0]; 
} 

my $sections = 128;
my $fbase="All.sha1c/commit_";
my %c2a;
if (! tie (%c2a,  'TokyoCabinet::HDB', "/fast1/${fbase}author.tch", TokyoCabinet::HDB::OREADER, 16777213, -1, -1, TokyoCabinet::TDB::TLARGE, 100000)){
  die "cant open ${fbase}author.tch\n";
}
my (%fhop);
my $pre = "/fast1";
my $sec = $ARGV[0];
#for my $sec (0..127){
tie %fhop, "TokyoCabinet::HDB", "$pre/${fbase}parent_$sec.tch", TokyoCabinet::HDB::OREADER,
      16777213, -1, -1, TokyoCabinet::TDB::TLARGE, 100000
   or die "cant open $pre/${fbase}parent_$sec.tch\n";

while (my ($sha, $v) = each %fhop){
  my @pp = extr ($v);    
  my ($auth, $cmtr, $ta, $tc) = split (/;/, $c2a{$sha});
  for my $p (@pp){
     if (length ($p) == 20){
       if (defined $c2a{$p}){
         my ($authp, $cmtrp, $tap, $tcp) = split (/;/, $c2a{$p});
         if ($authp ne $auth){
           print "$auth;$authp;".(toHex($sha)).";".(toHex($p))."\n";
         }
       }else{
         print "".(toHex($p))."\n";
       }
     }
  }
}
untie %fhop;
untie %fhoa;

sub extr {
  my $v = $_[0];
  #my $v1 = unpack "H*", $v;
  my $v1 = $v;
  my $n = length($v1);
  my @v2 = ();
  if ($n >= 20){
    for my $i (0..($n/20-1)){
      $v2[$i] = substr ($v1, $i*20, 20);
    }
  }
  return @v2;
}
```

#Lets investigate the largest component of connected commits
```
gunzip -c c2c.gz | perl connectExportUni.perl c2c
gunzip -c c2c.versions | ~/bin/connectD  482290078 | gzip > c2c.cloneD
gunzip -c c2c.cloneD | perl connectImport1.perl c2c | gzip > c2c.mapD
#The largest components are:
gunzip -c c2c.mapD | cut -d\; -f2 | uniq -c | lsort 10G -rn | head -30
6376239 3833ee97ad1cd7d366ac6ea8ea875d88d7f0cfb1
5421638 e9f6029ed5f91b6a39bc04e8eeda58181ae6d51a
4668662 c1bed3771554c23cf9922e163677278c946518a9
3812679 e7d99513d9e5cf7c52677af6a0cbbb2f46c66f2d
2816973 1677cf000ee404c45b57d3e87124ba9cc9ee2395
2207451 139c68f3288ff0894e08d7e367790b44b06ba72f
1846085 2c025ff2cee7c31757c51f060b409a8240ac81e6
1689415 581426feab53ff8e016fa729b422e26421c627ca
1582457 b7977708883f064ca62660fe8333e7be6c407e0f
1571682 602e388da2106abefd10f425ab07ecc93a99d007
1434290 698caf208b4aed32570ade216a09bc304482840c
1394468 dd603bfc33bbc5b33d4175f927fca03ab6f35025
 963613 14a0e58074f2698829b6554f578e6762c377caa3
 907571 68a9f130ae223ad4ede7926a3c5dee607a76b876
 886922 75f771e3d97ecd7670dcc0c39ccf10cad1c0b268
 840590 e7160af40f523d8874b1df64d6ea976bfecfde0f
 835747 c3ecf748550c204db4455a9f08851a18dabeadca
 699398 dffdf45b2daaa086e72547de0a2c8cc7c83b2037
 635680 ce08508d2293cdc8b436dae739bba4927ca2a1d7
 589059 be70f030d6a3b414dce96f4f34a5188a45090921
 580242 ad64f34e4da291da0f8a83551523dabfba753a41
 519561 ea4e8108e61f1f7069a5f1c206164950dc21adda
 515296 587d2ab3dec1b207e0cf41aa4b0d6621cd32281f
 494408 83b8eca5e53c53e7eaf75237de4a13598c2f5403
 493441 8d821f0fd334739b04b9a20a4d6ca0e84a38da01
 458717 bbcd59be57a4ea61d2fc1ab1cd1292d7388ca521
 457664 1b93f571046e5629950f4c9c5ad8f5f8616ab348
 443796 f76225faff20acdcb13934aae120efc9af3b30e3
 440678 d0611983d1262bba5a909de3aafba9f27925afce
 440405 1079922c3fd9cbb617265754ee6ca2f3faf202f6

#Lets investigate what are the large components: are they valid?
# also what is the distribution of the size of these?
# how many people are involved in larg/small?
gunzip -c c2pTrPaT.all.gz | grep 3833ee97ad1cd7d366ac6ea8ea875d88d7f0cfb1


#illustrates very large cluster of commits
perl -I ~/lib64/perl5 Cmt2Par.perl | gzip > c2child.gz
gunzip -c c2child.s.versions | ~/bin/connect 19020591 | gzip > c2child.s.clone
gunzip -c c2child.s.clone |perl connectImport1.perl c2child.s | gzip > c2child.s.map
gunzip -c c2pTrPaT.all.gz | perl -e 'open A, "gunzip -c c2child.s.map|"; while(<A>){chop();($a,$b)=split(/\;/);$m{$a}=$b;};while (<STDIN>){chop();@x=split(/\;/);print "$m{$x[0]};$x[1];$m{$x[3]};$x[4]\n";}' | gzip > canC2pTrPaT.all.gz
gunzip -c c2pTrPaT.all.shp.git.cluster.cmt | perl -e 'open(A, "gunzip -c c2child.s.map|");while(<A>){chop();($a,$b) = split (/;/); $m{$a}=$b;} while (<STDIN>){chop();print "$_;$m{$a}\n";}' | gzip > c2pTrPaT.all.shp.git.cluster.cmtmap
```