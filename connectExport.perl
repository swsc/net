use warnings;
use strict;
use File::Temp qw/ :POSIX /;

print STDERR "starting ".(localtime())."\n";
open A, "|gzip>$ARGV[0].names";
open B, "|gzip>$ARGV[0].versions";
open C, "|gzip>$ARGV[0].ids";
my (%id2num, %f2num);
my $n = 0;
my $i = 0;
my ($pa, $pb) = (-1, -1);
while(<STDIN>){
	chop();
	my ($id, $v) = split(/\;/, $_, -1);
	if (!defined $f2num{$v}){
		$f2num{$v} = $i+0;
		print A "$v\n";
		$i++;
	}
	if (!defined $id2num{$id}){
		$id2num{$id} = $f2num{$v};
		print C "$id $f2num{$v}\n";
	}
	print B "$id2num{$id} $f2num{$v}\n" if $id2num{$id} != $f2num{$v} &&
		($id2num{$id} != $pa || $f2num{$v} != $pb);
	($pa, $pb) = ($id2num{$id}, $f2num{$v});
	$n ++;
	if (!($n%1000000000)) { print STDERR "$n lines done\n";}
}
print B "".($i-1)." ".($i-1)."\n";#ensure a complete list of vertices

