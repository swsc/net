#include <boost/config.hpp>
#include <iostream>
#include <boost/graph/use_mpi.hpp>
#include <boost/graph/distributed/mpi_process_group.hpp>
#include <boost/graph/distributed/adjacency_list.hpp>
#include <boost/graph/distributed/connected_components_parallel_search.hpp>
#include <boost/graph/distributed/connected_components.hpp>
#include <algorithm>
#include <vector>
#include <queue>
#include <limits>
#include <map>
#include <boost/graph/distributed/graphviz.hpp>
// #include <boost/graph/connected_components.hpp>
// #include <boost/graph/adjacency_list.hpp>
//using namespace std;
//extern std::istream cin;
//extern std::ostream cout;
int main (int argc, char ** argv){
	int gsiz = atoi(argv[1]);
	using namespace boost;
	using boost::graph::distributed::mpi_process_group;
	{
		mpi::environment env(argc, argv);
		mpi::communicator world;


		mpi_process_group pg;
		parallel::variant_distribution<mpi_process_group> distrib 
			      = parallel::block(pg, gsiz);

		typedef adjacency_list < vecS, 
				boost::distributedS<boost::graph::distributed::mpi_process_group, boost::vecS>, 
				undirectedS > Graph;
		
		Graph G (gsiz);
		
		if (process_id (G .process_group()) == 0) {
			unsigned int a, b, j = 0;
			std::cin>>a; std::cin>>b;
			//std::cout<<a<<b;
			while (!std::cin.eof()){
				add_edge (vertex(a, G), vertex(b, G), G);
				std::cin>>a;std::cin>>b;
				if (! ((j++)%1000000000)) std::cerr << j << std::endl;
			}	
			std::cerr << "done reading:" << j << std::endl;
		}

		synchronize(G);

		int nv = num_vertices(G);
		//std::cerr << "Created local graph " << process_id (G .process_group()) << 
		//	" with " << nv << " vertices" << std::endl;
		
		std::vector<long int> lcomponent (num_vertices(G));
		typedef iterator_property_map<std::vector<long int>::iterator, 
				property_map<Graph, vertex_index_t>::type> ComponentMap;
		ComponentMap component (lcomponent.begin(), get (vertex_index, G));
		//std::cerr << "Created components for local graph " << process_id (G .process_group()) <<
		//	" with " << nv << " vertices" << std::endl;
		
		int num = connected_components_ps (G, component);
		
		if (process_id(G.process_group()) == 0){
			//std::cerr << "Connected graph got " << num << " components" << std::endl;
			component.set_max_ghost_cells(0);
			for (int i = 0; i < gsiz; i++){
				int c = get (component, vertex(i, G));
				std::cout << i << ";" << c << std::endl;
			}
			synchronize(component);
		}else{
			synchronize(component);
		}
		//write_graphviz("cc.dot", G, paint_by_number(component));
	}
	return 0;
}
