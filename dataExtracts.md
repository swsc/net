Existing data extracts to speed graph construction
--------------------------------------------------

Data related to commits
=======================

 - pr2n.gz - a mapping from project to an author who made at least one commit to that project
```
 javasim;janlemeire;janlemeire
 phoenix-project;baojie;baojie
 zantastico;dragon52225;dragon52225
 jikesrvm;sjfink-oss;sjfink-oss
 jikesrvm;perry-oss;perry-oss
 jikesrvm;dgrove-oss;dgrove-oss
 feedmu;root;root
```
 
 - mid2n.gz - a mapping from message id to an author who used that message in at least one commit
```
  132545301;SmallTigerBrother;664674103@qq.com
  180742087;roever;roever
  90113932;Aaron Ortbals;aaron.ortbals@gmail.com
  4525620;simonpj;unknown
```

 - delta.idx.XX.gz - the commit metadata (from, e.g., git log) For each XX there is a corresponding
  
  1. XX.h2n.gz maps hash to name associated with that commit
 ```
   7fa6dc9f23004faa9d2fad5c5a8ae427caa7d5c6;John Lindgren;john.lindgren@aol.com;1
   37a272ad7ffe7bfd006d49d98f9b206d4958c753;James Ivings;james@ivings.name;1
   cc8adcc4b206709689c5ea6142fe49ff6f7c2682;phương Bui;hitechworm@gmail.com;1
   35672639df52c7205cdf8d3ee6a9f479e100b180;Luis Cruz;luis.cruz@ist.utl.pt;9
   9462dee21bfac36ee815cb2713f5dfac493527e1;Jon Parsons;jonparsons@gmail.com;2
 ```


Data related to content ids
===========================

 - cid1prj.gz - a subset of content ids that belonfg to a single project only (approx 550M or half of all content ids)

 - da3:/data/delta/all.idx.XX.gz - is data from da3:/data/bkp/All.new.idx.*.gz extractied into 0-45 1G record files
 
 - For each  all.idx.XX.gz there is a corresponding
   
   1. f2p.XX.gz - file to project name mapping (filw is specified in suffix order)
```
    link-template.php/wp-includes/fitnesswebsitegurus;github.com_laxmi59_fitnesswebsitegurus.git;1
    back.hpp/intrinsic/sequence/fusion/boost/include/x86/groovy/rosdeps/opt;github.com_ngocphu811_windows_ros-groovy.git;1
```   
   2. cid2pr.XX.gz - content id to project name
```
    918976257;github.com_malcolmgreaves_dotty.git;1
    918976257;github.com_VladimirNik_dotty.git;1
    918976257;github.com_leakingtapan_dotty.git;1
```
   3. h2cid.XX.gz - hash to content id mapping 
```
    83405601f467c85f42dbab3c654347298c6c0356;71247266;1
    ee6ca886248671adbbf29171477c239ea29da91a;153058106;5
    ee6ca886248671adbbf29171477c239ea29da91a;153058107;5
```


