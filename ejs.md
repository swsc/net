# Focus on validating blob search using emberjs/ember.js project

# on da4
cd /data
# investigate ejs data
```
gunzip -c /da3_data/delta/c2fbp.gz | grep -F emberjs_ember.js > ejs/ejs.c2fbp
```
# get all blobs from 20141225 batch 14
```
grep ';blob;' ~/github.com_emberjs_ember.js.git.olist|cut -d\; -f3 | sort -u > ejs/ejs.blob
wc ejs/ejs.c2fbp ejs/ejs.blob
   2217    2217  356683 ejs/ejs.c2fbp
  36911   36911 1513351 ejs/ejs.blob
```
#problem! too few in c2fbp.gz
#get all the funny files from git
```
grep ';blob;' ~/github.com_emberjs_ember.js.git.olist|cut -d\; -f4 | sort -u > ejs/ejs.f
```

# Try to find why
# traces of the old-style extractions
```
ls -f /da3_data/tch/New2014122514*  | while read i; do gunzip -c $i | grep emberjs_ember.js; done > ejs/ejs.New
cut -d\; -f3 ejs/ejs.New | sed 's|/\([^/]*\)$|;\1|' | sort -t\; -k2 > ejs/ejs.New.f2cmt
cut -d\; -f3 ejs/ejs.New | sed 's|.*/||' | lsort 10G -u > ejs/ejs.New.cmt
wc ejs/ejs.New.cmt
 11393  11393 467113 ejs/ejs.New.cmt
```
#No problem here:  reasonable number of commits (4 blobs   per commit)
```
wc ejs/ejs.New.f2cmt
  39762   39762 4803259 ejs/ejs.New.f2cmt
```
# file/commit should represent a new blob, so plenty

```
for i in {0..127}; do cut -d\; -f5- All.blobs/blob_$i.idx | grep ';' | cut -d\; -f1,3; done  | awk -F\; '{print $2";"$1}'|gzip > v2b.gz
gunzip -c v2b.gz | lsort 200G -t\; -k1b,1 | gzip > v2b.s
```

# An alternative to join: see if the v2b.gz contains all the blobs:
```
gunzip -c v2b.gz | grep -Ff ejs/ejs.blob > ejs/ejs.blob.v2b
wc ejs/ejs.blob.v2b
  34909   34909 1783429 ejs/ejs.blob.v2b
#No problem here, < 36911  in ejs/ejs.blob, but likely because of the different definition of a text file
#That means that either join did not work or all.idx.new.0-74.s2
#misses pieces of data
```

# lets make sure the /da3_data/delta/c2fbp.gz is correctly produced
```
gunzip -c v2b.s1 | grep -Ff ejs/ejs.blob > ejs/ejs.blob.v2b.s1
cat ejs/ejs.blob.v2b.s1 | cut -d\; -f3- | sed 's|/\([^/]*\)$|;\1|'| grep -E ';[^;]{40};' | sed 's|^[^/]*/||'|sed 's|^gitleft/||'|sed 's|/|;|' | awk -F\; '{print $3";"$2";"$4";"$1}' | lsort 10G -t\; -k1,1 > ejs/ejs.c2fbp1

# lets check all.idx.new.0-74.s2
gunzip -c all.idx.new.0-74.s2 | grep emberjs_ember.js > ejs/ejs.an
cut -d\; -f1 ejs/ejs.an | sort -u | wc
  33523   33523  337066
#That proves that the number of ejs blobs in all.idx.new.0-74.s2
# is approximately right (proviso whats considered as a text file)
```

# Try another approach match by metadata obtained via git log
```
gunzip -c /da1_data/delta/gitPnewnew20141225.14.deltaall.gz | grep emberjs_ember.js > ejs/ejs.delta
cut -d\; -f2 ejs/ejs.delta  | sort -u > ejs/ejs.delta.cmts
#now with the list of commits go over the blobs:
gunzip -c all.idx.new.0-74.s2 | grep -Ff ejs/ejs.delta.cmts > ejs/ejs.delta.cmts.an1
sed 's|;[0-9]*;.*/|;|' ejs/ejs.delta.cmts.an1 | lsort 10G -t\; -k1,1 > ejs/ejs.delta.id2cmts
cut -d\; -f1 ejs/ejs.delta.id2cmts | sort -u | wc
  33807   33807  339906
#number of unique ids appears to match 
#join that subset of ids to ids in v2b:
cut -d\; -f1  ejs/ejs.delta.id2cmts | sort -u > ejs/ejs.delta.ids
cat ejs/ejs.delta.ids | join -t\; - <(gunzip -c v2b.s) > ejs/ejs.delta.ids.v2b
wc ejs/ejs.delta.ids.v2b
  33392   33392 1704828 ejs/ejs.delta.ids.v2b
#clearly, all the ids and blobs are in v2b.s and in all.idx.new.0-74.s2, but the join appears to 
#fail...
```

#Redo the join to see what is going on
```
all.idx.new.0-74.s2 | join -t\; - <(gunzip -c v2b.s) | gzip > v2b.s1
#even worse: the v2b.s1 is much-much smaller than v2b.s1.old (also in /da3_data/delta)
#perhaps sort needs improvements:
gunzip -c v2b.gz | lsort 200G -t\; -k1,1 | gzip > v2b.s.k1.1
gunzip -c all.idx.new.0-74.s2 | join --check-order -t\; - <(gunzip -c v2b.s.k1.1) | gzip > v2b.s1.k1.1
ls -ltr v2b.s1* all.idx.new.0-74.s2 
-rw-rw-r--. 1 audris da 829248492907 Dec 24 21:04 all.idx.new.0-74.s2
-rw-rw-r--. 1 audris da 214013795296 Dec 25 22:59 v2b.s1.old
-rw-rw-r--. 1 audris da   4579882354 Feb 12 03:55 v2b.s1
-rw-rw-r--. 1 audris da 882304665252 Feb 15 05:57 v2b.s1.k1.1
#now the join makes motre sense, its bigger than all.idx.new.0-74.s2 as it should be
# v2b.s1.old and v2b.s1 have serious issues


#also followup at https://bitbucket.org/swsc/net/src/bc6d1cf5c84175533fe98ce9e0b45de2812531d2/graph.md?at=master&fileviewer=file-view-default
#also redo c2fbp.gz
gunzip -c v2b.s1.k1.1| cut -d\; -f3- | sed 's|/\([^/]*\)$|;\1|'| grep -E ';[^;]{40};' | sed 's|^[^/]*/||'|sed 's|^gitleft/||'|sed 's|/|;|' | awk -F\; '{print $3";"$2";"$4";"$1}' | lsort 200G -t\; -k1b,1 | gzip > c2fbp.gz
```
