Scripts to obtain connected components
======================================

One of the issues with perl and python is that they are slow for
certain tasks, such as iteration.

Many graph algorithms don't work well on large graphs. The first
step is to determine unconnected subgraphs. To create 
connected subgraphs we can use Boost library implementation
'connected_components' (see file connect.cpp).


Resilience
=========

Doing encoding, connection and decoding in one step
as described later in this document is inviting a disaster: what if some of the components 
run out of RAM?

connectExport.perl 
---------

This simply exports the graph (as .name and .versions files) 
in the integer format that is more space efficient and 
can be used by boost library directly.
For example, to obtain  project-induced network among developers:
```
cd /da3_data/delta
gunzip -c pr2n.gz | grep -v '^;' | sed 's/;/:/2'| \
  grep -E ';.+:.+\@.+$'  | perl connectExport.perl pr2n1
```
This step produces pr2n1.versions, pr2n1.ids, and pr2n1.names
The filter above ensures that the email has at least one character
before and after '@' and the name has at least one character.

The actual algorithm runs here:
```
gunzip < pr2n1.versions | ./connect |gzip > pr2n1.clones
```

Finally we map integer ids back  to names we can read:
```
perl connectImport.perl pr2n1 | gzip > pr2n1.map
```

Whats the biggest connected subcomponent?
```
gunzip -c pr2n1.map | cut -d\; -f2 | sort | uniq -c | sort -n | tail
    256 ke:ke@f57ce169-c725-0410-9505-c4955b3f21d2
    322 Bj:none@example.com
    450 Ava Beer:ava_beer@schoen.org
    538 http://lkcl.net/ <lkcl@web>:lkcl
    676 Arne Dare:arne.dare@dach.biz
   1198 J:uid-818@git.partyvan.info
   2174 Sz:uid-123@git.de.wikibooks.org
   2382 Bo Rath:rath.bo@simonis.com
  34001 Jess Orn:jess_orn@kuhn.com
4548065 z:z@y
```
It is the one where the name and email is omitted :-).


Another example, to get a network of blobs (content IDs) to developer
#################################################

we can first obtain commit hash to name mapping:

```
cd /da3_data/delta
seq 0 33 | while read i; do
    gunzip -c delta.idx.$i.gz | cut -d\; -f3,4,5 | uniq | gzip > cmt2n.$i.gz;
done
```
Then the blob to commit map
```
cd /da3_data/
for i in {0..127}; do
    cut -d\; -f5,7 All.blobs/blob_$i.vs | grep -v ^New | sed 's|;.*/|;|' | gzip > b2c.$i.gz;
done
```
After joining all cmt2n.$i.gz with b2c.$i.gz
we will get an answer. Joining can be done in RAM as all
b2c.$i.gz take 70G and one of cmt2n.$i.gz is 30G.
```
cd /da3_data/delta
for i in {0..127}; do gunzip -c ../b2c.$i.gz
done | uniq | awk -F\; '{print $2";"$1}' | lsort 220G -t\; -k1,2 | gzip > b2c.s

#exclude versions that are not commits (length 40)
for i in {0..34}; do
  gunzip -c cmt2n.$i.gz | perl -ane 'print if m/^[^;]{40};/' |
    lsort 50G -t\; -k1,2 | gzip > cmt2n.$i.s
done
gunzip -c cmt2n.0.s | lsort 50G -t\; -k1,2 --merge - <(gunzip -c cmt2n.1.s) \
 <(gunzip -c cmt2n.2.s) <(gunzip -c cmt2n.3.s) <(gunzip -c cmt2n.4.s) <(gunzip -c cmt2n.5.s) \
 <(gunzip -c cmt2n.6.s) <(gunzip -c cmt2n.7.s) <(gunzip -c cmt2n.8.s) <(gunzip -c cmt2n.9.s) \
 <(gunzip -c cmt2n.10.s) <(gunzip -c cmt2n.11.s) <(gunzip -c cmt2n.12.s) <(gunzip -c cmt2n.13.s) \
 <(gunzip -c cmt2n.14.s) <(gunzip -c cmt2n.15.s) <(gunzip -c cmt2n.16.s) <(gunzip -c cmt2n.17.s) \
 <(gunzip -c cmt2n.18.s) <(gunzip -c cmt2n.19.s) <(gunzip -c cmt2n.20.s) <(gunzip -c cmt2n.21.s) \
 <(gunzip -c cmt2n.22.s) <(gunzip -c cmt2n.23.s) <(gunzip -c cmt2n.24.s) <(gunzip -c cmt2n.25.s) \
 <(gunzip -c cmt2n.26.s) <(gunzip -c cmt2n.27.s) <(gunzip -c cmt2n.28.s) <(gunzip -c cmt2n.29.s) \
 <(gunzip -c cmt2n.30.s) <(gunzip -c cmt2n.31.s) <(gunzip -c cmt2n.32.s) <(gunzip -c cmt2n.33.s) \
 <(gunzip -c cmt2n.34.s) |uniq | gzip > cmt2n.s &

#some of the cmt2n.s have cvs/other data
gunzip -c cmt2n.s|uniq | join -t\; - <(gunzip -c b2c.s) | gzip > cmt2nb.s

#Finally count blobs (its a file/version) with two authors
gunzip -c cmt2nb.s | awk -F\; '{print $4";"$2";"$3}' | lsort 250G -t\; -k1,2 | uniq| gzip > b2n.s

#one author
gunzip -c b2n.s | cut -d\; -f1 | uniq -u | wc
165,927,529

#only 10% of the 1.3B blobs
#now several authors
gunzip -c b2n.s | cut -d\; -f1 | uniq -d | wc
16,353,587

#If we tie these blobs into version history of a file they
#represent, the multi-author fraction would increase dramatically.
#Not surprisingly, much less. But where are 90% of the 1.3B blobs:
#not connected via commits?

for i in {0..127}; do tail -1 /data/All.blobs/blob_$i.idx| cut -d\; -f1; done | awk '{print i+=$1}' | tail -1
1322583953

#lets check the number of blobs
for i in {0..127}; do cut -d\; -f5 ../All.blobs/blob_$i.vs | grep -v ^New; done  | lsort 200G -u | wc
202,665,933
#Ok way too little

#one way to use .vs files in da3:/data/cmts and da4:/data/cmts
...

#lets try another approach
for i in {0..127}
do cut -d\; -f5- ../All.blobs/blob_$i.idx | grep ';' | cut -d\; -f1,3
done  | awk -F\; '{print $2";"$1}'| lsort 200G -t\; -k1,1 | gzip > v2b.si.old

gunzip -c /da4_data/all.idx.new.0-74.s2 | join -t\; - <(gunzip -c v2b.s.old) | gzip > v2b.s1
```
# ** The join for some reson failed without any error message **
# ** see [ejs.md](https://bitbucket.org/swsc/net/src/master/ejs.md) **

```
#now we have filename/version to blob map
#need to link it back to authors:
# a) when commit sha is the version, map by it
# note this ran for a few weeks on da4, most likely because of grep
#-E, see massively faster  length($x[1]) == 40) alternative
cd /da4_data
gunzip -c v2b.s1.k1.1 | cut -d\; -f3- | sed 's|/\([^/]*\)$|;\1|'| \
  grep -E ';[^;]{40};' | sed 's|^[^/]*/||'|sed 's|^gitleft/||'|sed 's|/|;|' |\
  awk -F\; '{print $3";"$2";"$4";"$1}' | gzip > c2fbp.gz
gunzip -c c2fbp.gz | split -l 1000000000 --filter='gzip > $FILE.gz' --numeric-suffixes=0 - c2fbp.

# b) for non-git use filename
gunzip -c v2b.s1.k1.1 | cut -d\; -f3- | sed 's|/\([^/]*\)$|;\1|'| \
  perl -ane '@x=split(/\;/, $_, -1); if (length($x[1]) != 40){s|^[^/]*/||;s|/|;|;s/\r//g;s/^\///;print};' | \
  awk -F\;  '{print $2";"$4";"$1";"$3}' | lsort 100G -t\; -k1,1 -u |gzip > f2bpv.gz
#in the above sed is too slow: with week+ no result
gunzip -c v2b.s1.k1.1 | cut -d\; -f3- | \
  perl -ane '@x=split(/\;/, $_, -1); $x[0] =~s|/([^/]*)$|;$1|;$h=$1;if (length($h) != 40){$x[0]=~s|^[^/]*/||;$x[0]=~s|/|;|;$x[0]=~s/\r//g;$x[0]=~s/^\///;print"".(join ";", @x);};' | \
awk -F\; '{print $2";"$4";"$1";"$3}' | uniq | gzip > f2bpv.gz
gunzip -c f2bpv.gz | lsort 100G -t\; -k1,1 -u |gzip > f2bpv.s

# Now need to get the same from the metadata, use modified /da3_data/delta/run.sh

```



Yet another example, to get all 
content id to project mappings we can do:

```
seq 0 45 | while read i; do gunzip -c cid2pr.$i.gz; done | \
  perl connectExport.perl c2pNew
```

After running the boost algorithm:
```
gunzip < c2pNew.versions | $ENV{HOME}/bin/connect |gzip > c2pNew.clones
```

The results can be converted back to text via 
```
perl connectImport.perl c2pNew | gzip > c2pNew.map
```



Two-type vertex (bipartite) graph
--------------------
To do that we will read a graph that has two types of vertices:
 - cid - used as an implicit link between regular vertexes that share it
 - regular vertexes

For example we can create a connected subset via connectPbyCID.perl:
```
  gunzip -c /da3_data/delta/cid2pr.[0-1].gz | \
   perl /da3_data/delta/connectPbyCID.perl | \
   gzip > /da3_data/delta/cid2pr.0-1.clone.gz
```

uses input in cid2pr.xx.gz:
```
46386889;github.com_raichoo_illumos-gate.git;1
320421074;github.com_aalmegaard_superslides.git;1
320421074;github.com_1bigidea_superslides.git;1
```
The cid 320421074 links regular vertices
github.com_aalmegaard_superslides.git and
github.com_1bigidea_superslides.git.

It produces cid2pr.0-1.clone.gz where only the regular vertexes
are compactly mapped v1nam;cluster name
in a one-type vertex graph.
```
github.com_aalmegaard_superslides.git;urc.git
github.com_1bigidea_superslides.git;urc.git
```
In this case both of the projects belong to
cluster urc.git.



One-type vertex graph
----------------------

We may have multiple one-type vertex graphs produced, for example

```
gunzip -c /da3_data/delta/f2p.[0-1].gz | \
  perl /da3_data/delta/connectPbyCID.perl | \
  gzip > /da3_data/delta/f2p.0-1.clone.gz
```
produces links among projects based on matching file names. 


To combine these graphs into a single graph
we use a single-type vertex merging via connectMerge.perl:
```
  gunzip -c /da3_data/delta/{cid2pr,f2p}.0-1.clone.gz  | \
   perl /da3_data/delta/connectMerge.perl | \
   gzip > /da3_data/delta/merge.0-1.clone.gz
```

Lets analyze these graphs:
```
gunzip -c /da3_data/delta/f2p.0-1.clone.gz| cut -d\; -f2 | uniq | wc
  32322   32322 1162562
gunzip -c /da3_data/delta/cid2pr.0-1.clone.gz | cut -d\; -f2 | uniq | wc
  41718   41718 1516816
gunzip -c /da3_data/delta/merge.0-1.clone.gz | cut -d\; -f2 | uniq | wc
  30616   30616 1114775
gunzip -c /da3_data/delta/f2p.[0-1].gz | cut -d\; -f2 | sort -u | wc
 185593  185593 6783166

```

Filename induced graph produces 32322 clusters, while
cid based graph induces 41718 clusters. By merging them
we get 30616 clusters. This is a six-time reduction from
185593 projects in f2p.[0-1].gz.

If we ignore the potential links introduced by cid or file
among different XX, the result:
```
gunzip -c /da3_data/delta/{cid2pr,f2p}.[01].clone.gz | \
perl /da3_data/delta/connectMerge.perl | \
gzip > /da3_data/delta/merge.0-1.pieces.clone.gz
gunzip -c /da3_data/delta/merge.0-1.pieces.clone.gz | cut -d\; -f2 | uniq | wc
33248
```
The number of extra clusters is almost 3K or
10% larger.

connectPbyCID.perl 
==================
Compress input/output:
```
open B, "gunzip < $tmp.clones|";
open A, "|gzip>$tmp.names";
```


Here is what the wrapper does. It first encodes strings as integers
for the project name:
```
my ($id, $v) = split(/\;/, $_, -1);
if (!defined $f2num{$v}){        
   $f2num{$v} = $i+0; 
   print A "$v\n";
   $i++;
}
```

Now encode the content ID as the id for the first
regular vertex it is attached:
```
if (!defined $id2num{$id}){
  $id2num{$id} = $f2num{$v};
}
```

Finally print the link:
```
print B "$id2num{$id} $f2num{$v}\n";
```

The file $tmp.versions contains the adjacencies that
will be read and the connected components produced by
[connect.cpp](https://bitbucket.org/eveng/tasks/src/master/connect.cpp)
(obtained via g++ -O3 -o connect connect.cpp)
```
system ("gunzip < $tmp.versions | $ENV{HOME}/bin/connect |gzip > $tmp.clones");
```

What remains to be done is to create connected subgraphs as hashes:
```
open B, "gunzip < $tmp.clones|";
while (<B>){
   chop();
   my ($f, $cl) = split(/\;/, $_, -1);
   $f=$f+0; $cl=$cl+0;
   $cluster{$cl}{$f}++;
}
```

And decode them:
```
while (my ($k, $v) = each %cluster){
   my %fs = ();
   for my $f (keys %{$v}){
      $fs{$num2f[$f]}++;
   }
   output (\%fs);
}
```

Use the shortest element as the label for the connected subset:
```
sub output {
   my $cl = $_[0];
   my @fs = sort { length($a) <=> length($b) } (keys %{$cl});
   for my $i (0 .. $#fs){
      print "$fs[$i]\;$fs[0]\n";
   }
}
```

connectMerge.perl 
==================

The only difference is that the IDs on both sides
are from the final vertex set:
```
my ($id1, $id2) = split(/\;/, $_, -1);
for my $v ($id1, $id2){
   if (!defined $f2num{$id1}){
      $f2num{$v} = $i+0;
      print A "$v\n";
      $i++;
   }
}
```


Performance
===========
c2pNew.versions and f2pNew.versions edge files contain a lot of redundant
edges. The boost algorithm has to store each edge separately taking up 
precious RAM space.

connectPrune.per simply reads edges and removes duplicates:
```
gunzip -c c2pNew.versions | perl connectPrune.perl | gzip > c2pNew.versions1
```

The pruned c2pNew.versions1 is processed many times faster:
```
gunzip < c2pNew.versions1 | $ENV{HOME}/bin/connect |gzip > c2pNew.clones
```

Distributed memory processing
=============================

This is described in [openmpi.md](https://bitbucket.org/eveng/tasks/src/master/openmpi.md)



